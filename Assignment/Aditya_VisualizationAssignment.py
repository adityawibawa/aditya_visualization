#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Import Numpy
import numpy as np
x = np.arange(0,100)
y = x*2
z = x**2


# In[2]:


#Import Matplotlib
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

# if we use other than jupyter we need to use plt.show() for every end command when we create a visual


# In[3]:


#Exercise 1
fig = plt.figure()
new_axis_ax = fig.add_axes([0,0,1,1])
new_axis_ax.plot(x,y,'b')
new_axis_ax.set_xlabel('x')
new_axis_ax.set_ylabel('y')
new_axis_ax.set_title('title')


# In[4]:


#Exercise2
print('hi')


# In[5]:


fig = plt.figure()
ax1 = fig.add_axes([0,0,1,1])
ax2 = fig.add_axes([0.2,0.5,.2,.2])


# In[6]:


ax1.plot(x,y,'r')
ax2.plot(x,y,'r')

fig


# In[7]:


#Exercise3
fig = plt.figure()
ax1 = fig.add_axes([0,0,1,1])
ax2 = fig.add_axes([0.2,0.5,.4,.4])


# In[8]:


ax1.plot(x,z,'b')
ax1.set_xlabel('x')
ax1.set_ylabel('z')

ax2.plot(x,y,'b')
ax2.set_xlabel('x')
ax2.set_ylabel('y')
ax2.set_xlim([20,22])
ax2.set_ylim([30,50])
ax2.set_title('zoom')

fig


# In[9]:


#Exercise4
fig,axes = plt.subplots(nrows=1, ncols=2)


# In[10]:


axes[0].plot(x,y,'b--',linewidth = 2)
axes[0].set_xlabel('x')
axes[0].set_ylabel('z')

axes[1].plot(x,z,'r',linewidth = 2)
axes[1].set_xlabel('x')
axes[1].set_ylabel('z')

fig


# In[11]:


fig,axes = plt.subplots(nrows=1, ncols=2, figsize=(15,3))

axes[0].plot(x,y,'b',linewidth = 4)
axes[0].set_xlabel('x')
axes[0].set_ylabel('z')

axes[1].plot(x,z,'r--',linewidth = 2)
axes[1].set_xlabel('x')
axes[1].set_ylabel('z')

fig


# In[12]:


import pandas as pd
import matplotlib.pyplot as plt
df3 = pd.read_csv('df3')
get_ipython().run_line_magic('matplotlib', 'inline')


# In[13]:


df3.info()


# In[14]:


df3.head()


# In[15]:


df3.plot.scatter(x='a',y='b',figsize=(20,5), color = 'red', edgecolor = 'black', s=200, xlim=([-0.2,1.2]), ylim=([-0.2,1.2]))


# In[16]:


df3['a'].hist(bins=10,color='blue',edgecolor = 'black', grid=False)


# In[17]:


plt.style.use('ggplot')


# In[18]:


df3['a'].plot.hist(bins=25, rwidth=0.9, alpha = 0.6)


# In[19]:


line_props = dict(color="r", alpha=0.3, linestyle="dashdot")
bbox_props = dict(color="g", alpha=0.9)
df3.boxplot(column=['a','b'], showcaps=False, whiskerprops=line_props, boxprops=bbox_props)


# In[22]:


df3['a'].plot.kde()


# In[24]:


df3['a'].plot.kde(linestyle='dashed')


# In[21]:


df3.plot.area(xlim=([0,30]), ylim=([0.0,3.0]), alpha = 0.4)


# In[ ]:




