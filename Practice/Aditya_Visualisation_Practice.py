#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[3]:


import numpy as np
x = np.linspace(0,5,11)
y = x**2


# In[4]:


x


# In[5]:


y


# In[9]:


plt.plot(x,y,'y')
plt.xlabel('X Axis Title Here')
plt.ylabel('Y Axis Title Here')
plt.title('String Title Here')
plt.show()


# In[19]:


plt.subplot(1,2,1)
plt.plot(x,y,'r--')
plt.subplot(1,2,2)
plt.plot(y,x,'g*-')


# In[20]:


fig = plt.figure()

axes = fig.add_axes([0.1,0.1,0.8,0.8])

axes.plot(x,y,'b')
axes.set_xlabel('Set X Label')
axes.set_ylabel('Set Y Label')
axes.set_title('Set Title')


# In[23]:


fig = plt.figure()

axes1 = fig.add_axes([0.1,0.1,0.8,0.8])
axes2 = fig.add_axes([0.2,0.5,0.4,0.3])

axes1.plot(x,y,'b')
axes1.set_xlabel('x_label_axes2')
axes1.set_ylabel('y_label_axes2')
axes1.set_title('Axes 2 Title')

axes2.plot(y,x,'r')
axes2.set_xlabel('x_label_axes2')
axes2.set_ylabel('y_label_axes2')
axes2.set_title('Axes 2 Title')


# In[24]:


fig, axes = plt.subplots()

axes.plot(x,y,'r')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('title')


# In[25]:


fig, axes = plt.subplots(nrows=1, ncols=2)


# In[26]:


axes


# In[27]:


for ax in axes:
    ax.plot(x,y,'b')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('title')
    
fig


# In[28]:


fig, axes = plt.subplots(nrows=1, ncols=2)

for ax in axes:
    ax.plot(x,y,'g')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('title')
    
fig
plt.tight_layout()


# In[29]:


fig = plt.figure(figsize=(8,4), dpi=100)


# In[30]:


fig, axes = plt.subplots(figsize=(12,3))

axes.plot(x,y,'r')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('title')


# In[31]:


fig.savefig("filename.png")


# In[32]:


fig.savefig("filename.png", dpi=200)


# In[34]:


ax.set_title("title");


# In[35]:


ax.set_xlabel('x')
ax.set_ylabel('y');


# In[36]:


fig = plt.figure()

ax = fig.add_axes([0,0,1,1])

ax.plot(x, x**2, label="x**2")
ax.plot(x, x**3, label="x**3")
ax.legend()


# In[37]:


ax.legend(loc=1)
ax.legend(loc=2)
ax.legend(loc=3)
ax.legend(loc=4)

ax.legend(loc=0)
fig


# In[38]:


fig, ax = plt.subplots()
ax.plot(x, x**2, 'b.-')
ax.plot(x, x**3, 'g--')


# In[40]:


fig, ax = plt.subplots()

ax.plot(x, x+1, color='blue', alpha=0.5)
ax.plot(x, x+2, color='#8B008B')
ax.plot(x, x+3, color='#FF8C00')


# In[45]:


fig, ax = plt.subplots(figsize=(12,6))

ax.plot(x, x+1, color='red', linewidth=0.25)
ax.plot(x, x+2, color='red', linewidth=0.50)
ax.plot(x, x+3, color='red', linewidth=1.00)
ax.plot(x, x+4, color='red', linewidth=2.00)

ax.plot(x, x+5, color='green', lw=3, linestyle='-')
ax.plot(x, x+6, color='green', lw=3, ls='-')
ax.plot(x, x+7, color='green', lw=3, ls=':')

line, = ax.plot(x, x+8, color='black', lw=1.50)
line.set_dashes([5, 10, 15, 10])

ax.plot(x, x+9, color='blue', lw=3, ls='-', marker='+')
ax.plot(x, x+10, color='blue', lw=3, ls='--', marker='o')
ax.plot(x, x+11, color='blue', lw=3, ls='-',marker='s')
ax.plot(x, x+12, color='blue', lw=3, ls='--',marker='1')

ax.plot(x, x+13, color='purple', lw=3, ls='-', marker='o', markersize=2)
ax.plot(x, x+14, color='purple', lw=3, ls='-', marker='o', markersize=4)
ax.plot(x, x+15, color='purple', lw=3, ls='-',marker='o',markersize=8, markerfacecolor='red')
ax.plot(x, x+16, color='purple', lw=3, ls='-',marker='s', markersize=8, markerfacecolor='yellow',
        markeredgewidth=3, markeredgecolor='green');


# In[47]:


fig, axes = plt.subplots(1,3, figsize=(12,4))

axes[0].plot(x,x**2,x,x**3)
axes[0].set_title('default axes ranges')

axes[1].plot(x,x**2,x,x**3)
axes[1].axis('tight')
axes[1].set_title('tight axes')

axes[2].plot(x,x**2,x,x**3)
axes[2].set_ylim([0,60])
axes[2].set_xlim([2,5])
axes[2].set_title('custom axes range');


# In[48]:


plt.scatter(x,y)


# In[49]:


from random import sample
data = sample(range(1,1000), 100)
plt.hist(data)


# In[51]:


data = [np.random.normal(0, std, 100) for std in range(1,4)]

plt.boxplot(data, vert=True, patch_artist=True);


# In[52]:


import numpy as np
import pandas as pd
get_ipython().run_line_magic('matplotlib', 'inline')


# In[53]:


df1 = pd.read_csv('df1', index_col=0)
df2 = pd.read_csv('df2')


# In[54]:


df1['A'].hist()


# In[55]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')


# In[56]:


df1['A'].hist()


# In[57]:


plt.style.use('bmh')
df1['A'].hist()


# In[59]:


plt.style.use('dark_background')
df1['A'].hist()


# In[60]:


plt.style.use('fivethirtyeight')
df1['A'].hist()


# In[67]:


plt.style.use('ggplot')
df2.head()


# In[68]:


df2.plot.bar()


# In[69]:


df2.plot.bar(stacked=True)


# In[70]:


df2.plot.area(alpha=0.4)


# In[71]:


df1['A'].plot.hist(bins=50)


# In[76]:


df1['Year'] = df1.index
df1.plot.line(x = 'Year', y='B', figsize=(12,3),lw=1)


# In[77]:


df1.plot.scatter(x='A', y='B')


# In[78]:


df1.plot.scatter(x='A', y='B', c='C', cmap='coolwarm')


# In[79]:


df1.plot.scatter(x='A', y='B', s=df1['C']*200)


# In[80]:


df2.plot.box()


# In[81]:


df = pd.DataFrame(np.random.randn(1000,2),columns=['a','b'])
df.plot.hexbin(x='a',y='b',gridsize=25,cmap='Oranges')


# In[82]:


df2['a'].plot.kde()


# In[83]:


df2.plot.density()


# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


mcdon = pd.read_csv('mcdonalds.csv', index_col='Date', parse_dates=True)


# In[3]:


mcdon.head()


# In[4]:


mcdon.plot()


# In[5]:


mcdon['Adj. Close'].plot()


# In[6]:


mcdon['Adj. Volume'].plot()


# In[7]:


mcdon['Adj. Close'].plot(figsize=(12,8))


# In[8]:


mcdon['Adj. Close'].plot(figsize=(12,8))
plt.ylabel('Close Price')
plt.xlabel('Overwrite Date Index')
plt.title('McDonalds')


# In[9]:


mcdon['Adj. Close'].plot(figsize=(12,8), title='Pandas Title')


# In[10]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2009-01-01'])


# In[11]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2009-01-01'],ylim=[0,50])


# In[12]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'],ylim=[0,40],ls='--',c='r')


# In[13]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates


# In[16]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'],ylim=[0,40])


# In[17]:


idx = mcdon.loc['2007-01-01':'2007-05-01'].index
stock = mcdon.loc['2007-01-01':'2007-05-01']['Adj. Close']


# In[18]:


idx


# In[19]:


stock


# In[20]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')
plt.tight_layout()
plt.show()


# In[21]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[22]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')
ax.yaxis.grid(True)
ax.xaxis.grid(True)
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[23]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('%b\n%Y'))


fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[24]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n\n%Y--%B'))


fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[26]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n%Y--%B'))

ax.xaxis.set_minor_locator(dates.WeekdayLocator())
ax.xaxis.set_minor_formatter(dates.DateFormatter('%d'))


fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[29]:


fig, ax = plt.subplots(figsize=(10,8))
ax.plot_date(idx, stock, '-')

ax.yaxis.grid(True)
ax.xaxis.grid(True)


ax.xaxis.set_major_locator(dates.WeekdayLocator(byweekday=1))
ax.xaxis.set_major_formatter(dates.DateFormatter('%B-%d-%a'))


fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[ ]:




